#!/usr/bin/python3
import os, sys, json
from random import random, uniform
import http.server, socketserver
from http import HTTPStatus
SimpleHTTPServer = http.server
SocketServer = socketserver

LOW_BOUND = -1000
HIGH_BOUND = 1000

OBJECTS = [ {'id':i, 'pos':[0,0,0], 'rot':[0,0,0]} for i in range(3000) ] 
DB = {'test':'data', 'objects':OBJECTS}

def simulate_server_side_logic():
	for ob in OBJECTS:
		ob['pos'][0] = round( uniform( LOW_BOUND, HIGH_BOUND ), 3)
		ob['pos'][1] = round( uniform( LOW_BOUND, HIGH_BOUND ), 3)
		ob['pos'][2] = round( uniform( LOW_BOUND, HIGH_BOUND ), 3)
		ob['rot'][0] = int( uniform(-360, 360) )
		ob['rot'][1] = int( uniform(-360, 360) )
		ob['rot'][2] = int( uniform(-360, 360) )

HTML = '''
<html>
<head>
<script>
var PREVTIME = Date.now();
function update_stream( data ) {
	var t = document.getElementById("TIME");
	while (t.firstChild) t.removeChild( t.firstChild );
	var now = Date.now();
	t.appendChild( document.createTextNode( "ms:" + (now-PREVTIME)) );
	PREVTIME = now;
	var pre = document.getElementById("DATA");
	while (pre.firstChild) pre.removeChild( pre.firstChild );
	pre.appendChild( document.createTextNode( data ) );
}

function get_stream(){
	var req = new XMLHttpRequest();
	var data = Math.random();
	req.open("GET", '/sync?{"foo":' + data + '}', true);
	req.setRequestHeader( 'Access-Control-Allow-Origin', '*');
	function onreply(){
		if (req.readyState == 4 && req.status==200){
			update_stream( req.responseText );
		}
	}
	req.onreadystatechange = onreply;
	req.send();
}

function start() {
	setInterval( get_stream, 100 );
}
</script>
</head>
<body onload="start()">
<h1 id="TIME">hello world</h1>
<hr/>
<div id="DATA" style="font-size:5px">
</div>
</body>
</html>
'''

class Handler(SimpleHTTPServer.SimpleHTTPRequestHandler):
	def log_message(self, format, *args):
		return

	def end_headers(self):
		self.send_header('Access-Control-Allow-Origin', '*')
		SimpleHTTPServer.SimpleHTTPRequestHandler.end_headers(self)

	def do_OPTIONS(self):
		self.send_response(200, "ok")
		#self.send_header('Access-Control-Allow-Origin', '*')
		self.send_header('Access-Control-Allow-Methods', 'GET, OPTIONS')
		self.send_header("Access-Control-Allow-Headers", "X-Requested-With")
		self.send_header("Access-Control-Allow-Headers", "Content-Type")
		self.send_header("Access-Control-Allow-Headers", 'Access-Control-Allow-Origin')
		self.end_headers()

	def do_GET(self):
		self.send_response(200)
		if self.path.endswith('.html') or self.path=='/':
			self.send_header('Content-type', 'text/html')
		else:
			self.send_header('Content-type', 'application/json')
		self.end_headers()

		#print(self.path)
		if self.path == '/':
			self.wfile.write( HTML.encode('utf-8') )
			
		## Amazon AWS EC2 requires at health `heart-beat` response
		elif self.path=='/health' or self.path.startswith('/health'):
			self.wfile.write( 'OK'.encode('utf-8') )

		## this is where data is sent to the server, and the server always replies with the entire DB
		elif self.path.startswith('/sync'):

			simulate_server_side_logic()

			if '?' in self.path and not self.path.endswith('?'):
				args = self.path.split('?')[-1]
				args = args.replace('%22', '"')
				updates = json.loads( args )

				if type(updates) is list:
					for item in updates:
						if type(item) is dict:
							if 'key' in item and 'value' in item:
								DB[ item['key'] ] = item['value']

				elif type(updates) is dict:
					DB.update( updates )

			msg = json.dumps(DB).encode('utf-8')
			print('sending kilobytes:', len(msg) / 1000.0  )
			self.wfile.write( msg )

		else:
			self.wfile.write( 'WARN: UNKNOWN REQUEST PATH'.encode('utf-8') )



httpd = SocketServer.TCPServer(("", 8081), Handler)
httpd.serve_forever()
