# simple_server_example

Running `crontab -e` will allow you to edit your cron.
Adding a line like this to it:

```bash
@reboot /path/to/server.py
```

will execute that script once your computer boots up.

# testing

open a webbrowser and go to:

```
http://localhost:8081/sync?{"foo":"bar"}
```

that will send the json data `{"foo":"bar"}` to the server's in memory database.
the server will respond by sending the entire database back to the client.
